<?php


namespace App\Http\Controllers;


use App\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TaskController extends Controller
{

    /**
     * TaskController constructor.
     */
    public function __construct()
    {
    }

    public function index()
    {
        return response()->json(Task::all() ,200);
    }

    public function show($id)
    {
        return response()->json(Task::find($id));
    }

    public function create(Request $request)
    {
        $task = Task::create($request->all());
        return response()->json($task, 201);
    }

    public function update($id, Request $request)
    {
        $task = Task::findOrFail($id);
        $task->update($request->all());
        return response()->json($task, 200);
    }

    public function delete($id)
    {
        $task = Task::findOrFail($id)->delete();
        return response()->json($task, 200);
    }
}
