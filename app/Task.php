<?php


namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{

    protected $fillable = [
        'title',
        'is_complete'
    ];

    protected $hidden = [];

    /**
     * Task constructor.
     */
}
